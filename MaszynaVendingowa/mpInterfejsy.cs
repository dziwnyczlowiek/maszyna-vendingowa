﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt1_Piekarski50405
{
    interface IPojemnikNaPieniadze
    {
        void MpGenerujPieniadze(int mpMinIloscBanknotow, int mpMaxIloscBanknotow);
        MpPojemnikNaPieniadze MpWyplacReszte(float mpKwota);
        bool MpWplacBanknot(float mpBanknot);
    }

    // To też raczej nie
    interface IPojemnikNaReszte
    {
        bool MpOdbierzReszte(decimal mpReszta);
        bool MpWydajReszte(decimal mpReszta);
        bool MpWyswietlPieniadze();
    }

    // To raczej nie
    interface IPortfel
    {
        // Todo
        decimal MpWplacPieniadze();
        // Todo
        bool MpWyswietlPieniadze();
    }

    interface IKomoraProduktow
    {
        bool MpWizualizujProdukty();
        bool MpOdbierzProdukty();
    }

    interface IInterfejsMaszyny
    {
        bool MpWybierzProdukt(int mpNumerProduktu);
        bool MpPrzelaczWalute();
        bool MpWyswietlCene();
        bool MpOtworzInterfejsTechniczny(string mpKodDostepu);
    }

    interface IInterfejsTechniczny
    {
        bool MpWyswietlZawartoscPojemnika();
    }

    interface IProdukt
    {
        int MpNumerProduktu { get; }
        string MpCenaProduktu { get; }
        string MpNazwaProduktu { get; }
        // Todo
        //bool MpPokazProdukt();
    }
}
