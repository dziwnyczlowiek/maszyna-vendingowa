﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Threading;

namespace Projekt1_Piekarski50405
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MpProdukt[] mpProdukty;
        private MpWaluta mpAktywnaWaluta;
        private int mpNumerAktywnejWaluty;
        private MpWaluta[] mpDostepneWaluty;
        private MpPojemnikNaPieniadze[] mpPojemnik;
        private string mpAccessCode = "*137";

        private MpProdukt mpKupowanyProdukt;
        private MpPojemnikNaPieniadze[] mpPojemnikNaWplaconePieniadze;
        private float mpWplaconaKwota = 0;
        private MpPortfel mpPortfel;
        private MpPojemnikNaPieniadze mpPojemnikNaReszte;
        private DispatcherTimer mpTimer = new DispatcherTimer();

        private MpOknoKonfiguracjiPojemnikow mpOknoKonfiguracji;

        public string MpAccessCode { get => mpAccessCode; }
        public MpProdukt[] MpProdukty { get => mpProdukty; }

        

        public MainWindow()
        {
            InitializeComponent();

            float[] mpBanknotyPLN = { 200, 100, 50, 20, 10, 5, 2, 1, 0.5F, 0.2F, 0.1F, 0.05F, 0.02F, 0.01F };
            float[] mpBanknotyEUR = { 500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5F, 0.2F, 0.1F, 0.05F, 0.02F, 0.01F };
            float[] mpBanknotyUSD = { 100, 50, 20, 10, 5, 2, 1, 0.5F, 0.25F, 0.1F, 0.05F, 0.01F };

            mpDostepneWaluty = new MpWaluta[3];

            mpDostepneWaluty[0] = new MpWaluta("PLN", mpBanknotyPLN, 1);
            mpDostepneWaluty[1] = new MpWaluta("EUR", mpBanknotyEUR, 0.22f);
            mpDostepneWaluty[2] = new MpWaluta("USD", mpBanknotyUSD, 0.26f);

            mpNumerAktywnejWaluty = 0;
            mpAktywnaWaluta = mpDostepneWaluty[mpNumerAktywnejWaluty];

            mpPojemnik = new MpPojemnikNaPieniadze[mpDostepneWaluty.Length];
            mpPojemnikNaWplaconePieniadze = new MpPojemnikNaPieniadze[mpDostepneWaluty.Length];
            for (int i = 0, iLength = mpDostepneWaluty.Length; i < iLength; i++)
            {
                if (mpDostepneWaluty[i] != null)
                {
                    mpPojemnik[i] = new MpPojemnikNaPieniadze(ref mpDostepneWaluty[i], 1, 10);
                    mpPojemnikNaWplaconePieniadze[i] = new MpPojemnikNaPieniadze(ref mpDostepneWaluty[i], 0, 0);
                }
            }

            MpProdukt rekawice = new MpProdukt("Rękawiczki", 3.99f);
            MpProdukt maska = new MpProdukt("Maska", 8.20f);
            MpProdukt przylbica = new MpProdukt("Przyłbica", 20);
            MpProdukt aspiryna = new MpProdukt("Aspiryna", 6.99f);
            MpProdukt respirator = new MpProdukt("Respirator", 31.40f);
            MpProdukt epinefryna = new MpProdukt("Epinefryna", 91.22f);
            MpProdukt dezynf = new MpProdukt("Płyn dezynfekujący", 11.99f);
            MpProdukt chust = new MpProdukt("Chusteczki dezynfekujące", 10);
            MpProdukt syrop = new MpProdukt("Syrop na kaszel", 20.10f);

            mpProdukty = new MpProdukt[] { rekawice, maska, przylbica, aspiryna, respirator, epinefryna, dezynf, chust, syrop };

            MpKomoraProduktow.MpWypelnijKomoreProduktow(mpProdukty);
        }

        private void MpInterfejsMaszyny_MpOkWcisniete(object sender, RoutedEventArgs e)
        {
            TekstInterfejsuArgs mpArgumenty = (TekstInterfejsuArgs)e;

            int mpNumerProduktu;

            if (mpArgumenty.MpTekstInterfejsu == mpAccessCode)
            {
                mpArgumenty.MpTekstInterfejsu = "DOSTĘP PRZYZNANY";
                mpArgumenty.MpTekstPrawidlowy = true;

                mpOknoKonfiguracji = new MpOknoKonfiguracjiPojemnikow(mpPojemnik);
                mpOknoKonfiguracji.Show();
                mpOknoKonfiguracji.Closed += MpOknoKonfiguracji_Closed;
                this.IsEnabled = false;

                return;
            }

            try
            {
                mpNumerProduktu = int.Parse(mpArgumenty.MpTekstInterfejsu);
            }
            catch
            {
                mpArgumenty.MpTekstInterfejsu = "Brak produktu";
                mpArgumenty.MpTekstPrawidlowy = false;
                return;
            }

            foreach (MpProdukt mpElement in mpProdukty)
            {
                if (mpElement.MpNumerProduktu == mpNumerProduktu)
                {
                    MpInterfejsMaszyny.mpWyswietlacz.MpWyswietlanyProdukt = mpElement;
                    mpArgumenty.MpTekstPrawidlowy = true;
                    mpArgumenty.MpTekstInterfejsu = mpElement.MpNazwaProduktu + "\n" 
                        + mpAktywnaWaluta.MpPrzeliczZeZlotowek(float.Parse(mpElement.MpCenaProduktu)).ToString("0.00")
                        + "\n" + mpAktywnaWaluta.MpSkrotWaluty;

                    // Zaktualizuj obecnie kupowany produkt
                    mpKupowanyProdukt = mpElement;

                    // Pokaż portfel z pieniędzmi do zapłaty
                    mpPortfel = new MpPortfel(mpDostepneWaluty);
                    mpPortfel.MpNominalKlikniety += MpPortfel_MpNominalKlikniety;
                    mpPortfel.MpZaplataKarta += MpPortfel_MpZaplataKarta;
                    mpPortfel.Closed += MpPortfel_Closed;
                    mpPortfel.Show();

                    return;
                }
            }

            mpArgumenty.MpTekstInterfejsu = "Brak produktu";
            mpArgumenty.MpTekstPrawidlowy = false;
        }

        private void MpOknoKonfiguracji_Closed(object sender, EventArgs e)
        {
            this.IsEnabled = true;
            MpInterfejsMaszyny.mpWyswietlacz.MpWyswietlWiadomosc(0);
        }

        private void MpPortfel_Closed(object sender, EventArgs e)
        {
            if (!MpInterfejsMaszyny.mpWyswietlacz.MpJestWyswietlany[2])
                MpInterfejsMaszyny.MpWcisnijAnuluj();
        }

        private void MpPortfel_MpZaplataKarta(object sender, EventArgs e)
        {
            MpFinalizujTransakcje(-1);;

            mpPortfel.Close();

            mpTimer.Interval = new TimeSpan(0, 0, 3);
            mpTimer.Tick += MpTimer_Tick;

            mpTimer.Start();
        }

        private void MpTimer_Tick(object sender, EventArgs e)
        {
            mpTimer.Stop();

            MpInterfejsMaszyny.mpWyswietlacz.MpWyswietlWiadomosc(0);

            MpInterfejsMaszyny.MpWcisnijAnuluj();

            MpInterfejsMaszyny_MpAnulujWcisniete(sender, e);
        }

        private void MpPortfel_MpNominalKlikniety(object sender, EventArgs e)
        {
            MpInterfejsMaszyny.btnZmienWalute.IsEnabled = false;

            MpWplataEventArgs mpWplata = (MpWplataEventArgs)e;

            foreach (MpPojemnikNaPieniadze mpPojemnik in mpPojemnikNaWplaconePieniadze)
            {
                if (mpPojemnik.MpRodzajWaluty.MpSkrotWaluty == mpWplata.MpSkrotWaluty)
                {
                    mpPojemnik.MpWplacBanknot(mpWplata.MpWybranyBanknot);

                    mpWplaconaKwota += mpWplata.MpWybranyBanknot / mpPojemnik.MpRodzajWaluty.MpWartoscZlotowki;

                    mpWplaconaKwota = (float)Math.Round(mpWplaconaKwota, 2);
                }
            }

            MpInterfejsMaszyny.mpWyswietlacz.MpJestWyswietlany[4] = true;

            MpInterfejsMaszyny.mpWyswietlacz.MpWyswietlanyTekst = "Wplacona kwota:\n"
                + mpAktywnaWaluta.MpPrzeliczZeZlotowek(mpWplaconaKwota).ToString("0.00") + "/"
                + mpAktywnaWaluta.MpPrzeliczZeZlotowek(float.Parse(mpKupowanyProdukt.MpCenaProduktu)).ToString("0.00")
                + "\n" + mpAktywnaWaluta.MpSkrotWaluty;

            if (mpWplaconaKwota >= float.Parse(mpKupowanyProdukt.MpCenaProduktu))
            {
                if (MpFinalizujTransakcje(mpWplaconaKwota))
                {
                    MpKupionyProduktIReszta mpOknoResztyIProduktu = new MpKupionyProduktIReszta(mpPojemnikNaReszte);

                    mpOknoResztyIProduktu.Show();
                }

                mpPortfel.Close();

                mpTimer.Interval = new TimeSpan(0, 0, 3);
                mpTimer.Tick += MpTimer_Tick;

                mpTimer.Start();

                return;
            }
        }

        private bool MpFinalizujTransakcje(float mpWplaconaKwota)
        {
            if (mpWplaconaKwota == -1)
            {
                MpInterfejsMaszyny.mpWyswietlacz.MpJestWyswietlany[5] = true;
                MpInterfejsMaszyny.mpWyswietlacz.MpWyswietlanyTekst = "Produkt zakupiony!\nDziękujemy za zakup.";
                return true;
            }

            float mpObliczonaReszta = mpAktywnaWaluta.MpPrzeliczZeZlotowek(mpWplaconaKwota)
                - mpAktywnaWaluta.MpPrzeliczZeZlotowek(float.Parse(mpKupowanyProdukt.MpCenaProduktu));

            foreach (MpPojemnikNaPieniadze mpPojemnikAktywnejWaluty in mpPojemnik)
            {
                if (mpPojemnikAktywnejWaluty.MpRodzajWaluty.Equals(mpAktywnaWaluta))
                {
                    mpPojemnikNaReszte = mpPojemnikAktywnejWaluty.MpWyplacReszte(mpObliczonaReszta);
                    break;
                }
            }

            mpObliczonaReszta = (float)Math.Round(mpObliczonaReszta, 2);
            float mpTest = (float)Math.Round(mpPojemnikNaReszte.MpPoliczPieniadze(), 2);

            if (mpObliczonaReszta == mpTest)
            {
                MpInterfejsMaszyny.mpWyswietlacz.MpJestWyswietlany[5] = true;
                MpInterfejsMaszyny.mpWyswietlacz.MpWyswietlanyTekst = "Produkt zakupiony!\nDziękujemy za zakup.";
                return true;
            }

            MpInterfejsMaszyny.mpWyswietlacz.MpWyswietlanyTekst = "Brak możliwości wydania reszty.\n"
                + "Spróbuj wprowadzić inną kwotę.";
            foreach (MpPojemnikNaPieniadze mpPojemnikAktywnejWaluty in mpPojemnik)
            {
                if (mpPojemnikAktywnejWaluty.MpRodzajWaluty.Equals(mpAktywnaWaluta))
                {
                    foreach (MpPlikBanknotow mpPlikBanknotow in mpPojemnikNaReszte.MpPlikiBanknotow)
                    {
                        while (mpPlikBanknotow.MpIloscBanknotow > 0)
                        {
                            mpPojemnikAktywnejWaluty.MpWplacBanknot(mpPlikBanknotow.MpWartoscBanknotu);
                            mpPlikBanknotow.MpIloscBanknotow--;
                        }
                    }
                    break;
                }
            }
            return false;
        }

        private void MpInterfejsMaszyny_MpZmienWalute(object sender, RoutedEventArgs e)
        {
            mpNumerAktywnejWaluty = (mpNumerAktywnejWaluty + 1) % mpDostepneWaluty.Length;

            mpAktywnaWaluta = mpDostepneWaluty[mpNumerAktywnejWaluty];

            MpKomoraProduktow.MpWyswietlCeneWWalucie(mpAktywnaWaluta);

            ZmianaWalutyArgs mpArgumenty = (ZmianaWalutyArgs)e;

            mpArgumenty.mpAktywnaWalutaPoZmianie = mpAktywnaWaluta;
        }

        private void MpInterfejsMaszyny_MpAnulujWcisniete(object sender, EventArgs e)
        {
            mpWplaconaKwota = 0;

            if (!(MpInterfejsMaszyny.mpWyswietlacz.MpJestWyswietlany[0] || MpInterfejsMaszyny.mpWyswietlacz.MpJestWyswietlany[1]
                || MpInterfejsMaszyny.mpWyswietlacz.MpJestWyswietlany[3]))
                mpPortfel.Close();
        }
    }
}
