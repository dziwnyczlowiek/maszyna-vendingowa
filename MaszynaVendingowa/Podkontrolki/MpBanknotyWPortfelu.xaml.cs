﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace Projekt1_Piekarski50405
{
    /// <summary>
    /// Interaction logic for MpBanknotyWPortfelu.xaml
    /// </summary>
    public partial class MpBanknotyWPortfelu : UserControl
    {
        public static readonly RoutedEvent MpNominalKliknietyEvent = EventManager.RegisterRoutedEvent("MpNominalKliknietyEvent",
            RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(MpBanknotyWPortfelu));

        public event RoutedEventHandler MpNominalKlikniety
        {
            add { AddHandler(MpNominalKliknietyEvent, value); }
            remove { RemoveHandler(MpNominalKliknietyEvent, value); }
        }

        public ObservableCollection<MpBanknot> MpBanknoty { get; set; }
        private MpWaluta mpWaluta;

        public MpBanknotyWPortfelu(MpWaluta mpWaluta)
        {
            InitializeComponent();

            this.mpWaluta = mpWaluta;

            MpBanknoty = new ObservableCollection<MpBanknot>();

            foreach (float mpNominal in mpWaluta.MpWartosciNominalow)
            {
                string mpSciezkaObrazu = $"/Obrazy/Banknoty/{mpWaluta.MpSkrotWaluty}/{mpNominal}.png";

                MpBanknoty.Add(new MpBanknot { MpNominal = mpNominal.ToString(), MpSciezkaObrazu = mpSciezkaObrazu });
            }

            DataContext = this;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button mpPrzycisk = (Button)sender;

            RaiseEvent(new MpWplataEventArgs(MpNominalKliknietyEvent, this) { MpWybranyBanknot = float.Parse(mpPrzycisk.Tag.ToString()),
            MpSkrotWaluty = mpWaluta.MpSkrotWaluty });
        }
    }

    public class MpBanknot
    {
        public string MpNominal { get; set; }
        public string MpSciezkaObrazu { get; set; }
    }

    public class MpWplataEventArgs : RoutedEventArgs
    {
        public float MpWybranyBanknot { get; set; }

        public string MpSkrotWaluty { get; set; }

        public MpWplataEventArgs(RoutedEvent routedEvent, object source) : base(routedEvent, source)
        {

        }
    }
}
