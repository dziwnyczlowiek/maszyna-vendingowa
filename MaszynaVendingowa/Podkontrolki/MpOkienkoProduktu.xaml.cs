﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Projekt1_Piekarski50405
{
    /// <summary>
    /// Interaction logic for MpOkienkoProduktu.xaml
    /// </summary>
    public partial class MpOkienkoProduktu : UserControl
    {
        public MpOkienkoProduktu()
        {
            InitializeComponent();

            MpWlasciwosci = new MpWlasciwosciNotyfikujace();

            this.DataContext = this;
        }

        int numerProduktu;

        public int NumerProduktu
        {
            get => numerProduktu;

            set
            {
                numerProduktu = value;

                SciezkaObrazka = $"/Obrazy/{numerProduktu}.png";
            }
        }

        public string NazwaProduktu { get; set; }

        public string SciezkaObrazka { get; private set; }

        public MpWlasciwosciNotyfikujace MpWlasciwosci { get; set; }

        public class MpWlasciwosciNotyfikujace : INotifyPropertyChanged
        {
            private string mpCenaProduktu;

            public event PropertyChangedEventHandler PropertyChanged;

            public string CenaProduktu
            {
                get => mpCenaProduktu;

                set
                {
                    mpCenaProduktu = value;
                    MpZmianaWlasciwosci("CenaProduktu");
                }
            }

            protected void MpZmianaWlasciwosci(string mpNazwaWlasciwosci)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(mpNazwaWlasciwosci));
            }
        }
    }
}
