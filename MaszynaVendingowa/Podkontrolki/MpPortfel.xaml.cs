﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace Projekt1_Piekarski50405
{
    /// <summary>
    /// Interaction logic for MpPortfel.xaml
    /// </summary>
    public partial class MpPortfel : Window
    {
        public delegate void MpNominalKliknietyHandler(object sender, MpWplataEventArgs e);
        public event EventHandler MpNominalKlikniety;
        public event EventHandler MpZaplataKarta;

        public MpPortfel(MpWaluta[] mpDostepneWaluty)
        {
            InitializeComponent();

            foreach (MpWaluta mpWaluta in mpDostepneWaluty)
            {
                MpBanknotyWPortfelu mpBanknotyWPortfelu = new MpBanknotyWPortfelu(mpWaluta);

                mpBanknotyWPortfelu.MpNominalKlikniety += MpPrzekazEventDalej;

                tcWaluty.Items.Add(new TabItem() { Header = mpWaluta.MpSkrotWaluty,
                    Content = mpBanknotyWPortfelu });
            }
        }

        public void MpPrzekazEventDalej(object sender, RoutedEventArgs e)
        {
            if (MpNominalKlikniety == null)
                return;

            MpNominalKlikniety(sender, e);
        }

        private void btnKartaPlatnicza_Click(object sender, RoutedEventArgs e)
        {
            MpZaplataKarta(sender, e);
        }
    }

    public class MpZakladka
    {
        public string MpNazwaWaluty { get; set; }
        public ObservableCollection<float> MpNominaly { get; set; }
    }
}
