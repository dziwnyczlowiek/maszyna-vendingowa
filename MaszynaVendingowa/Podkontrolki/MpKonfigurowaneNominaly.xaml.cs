﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace Projekt1_Piekarski50405
{
    /// <summary>
    /// Interaction logic for MpKonfigurowaneNominaly.xaml
    /// </summary>
    public partial class MpKonfigurowaneNominaly : UserControl
    {
        public MpPlikBanknotow[] MpNominaly { get; set; }
        public ObservableCollection<MpKonfigurowaneNominalyVM> MpListaNominalow { get; set; } = new ObservableCollection<MpKonfigurowaneNominalyVM>();

        public MpKonfigurowaneNominaly(MpPlikBanknotow[] mpNominaly)
        {
            InitializeComponent();

            DataContext = this;

            this.MpNominaly = mpNominaly;

            foreach (MpPlikBanknotow mpNominal in mpNominaly)
            {
                MpListaNominalow.Add(new MpKonfigurowaneNominalyVM() { MpNominal = mpNominal.MpWartoscBanknotu + " " + mpNominal.MpWaluta + ":",
                MpLiczbaNominalow = mpNominal.MpIloscBanknotow});
            }
        }

        public class MpKonfigurowaneNominalyVM
        {
            public string MpNominal { get; set; }
            public int MpLiczbaNominalow { get; set; }
        }
    }
}
